import pytest
import impala_health_check


def test_project_defines_author_and_version():
    assert hasattr(impala_health_check, '__author__')
    assert hasattr(impala_health_check, '__version__')
