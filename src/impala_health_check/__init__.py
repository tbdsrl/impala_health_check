import logging
import sys
import traceback
import socket
import os

from impala_health_check.utils import DBWrapper
from statsd import StatsClient

try:
    from flask import Flask
except ImportError:
    print "Please install flask first 'pip install flask'"
    sys.exit(1)

from flask import abort

app = Flask(__name__)

TARGET_HOST = os.environ.get('TARGET_HOST', 'localhost')
TARGET_PORT = os.environ.get('TARGET_PORT', '21050')
TIMEOUT_SECS = int(os.environ.get('TIMEOUT_SECS', 3))

TARGET_HOST_ID = socket.gethostname() if TARGET_HOST == 'localhost' else TARGET_HOST

STATSD_HOST = os.environ.get('STATSD_HOST', "localhost") # 'instal-grafana.c.feisty-gasket-100715.internal'
STATSD_PORT = os.environ.get('STATSD_PORT', 8125)
STATSD_PREFIX = os.environ.get('STATSD_PREFIX', "impala_health_check.temp")

STATSD_CLIENT = StatsClient(host=STATSD_HOST,
                     port=STATSD_PORT,
                     prefix=STATSD_PREFIX,
                     maxudpsize=512)

@app.route("/health")
def health():
    try:
        try:
            db = DBWrapper(TARGET_HOST, TARGET_PORT, TIMEOUT_SECS)
            with db.execute("select 1 as HEALTH_CHECK from _impala_health_check") as cursor:
                logging.debug( db.parse_data(cursor))
            STATSD_CLIENT.incr("{}.ok".format(TARGET_HOST_ID))
            return "ok"
        finally:
            db.close_connection()
    except Exception, e:
        STATSD_CLIENT.incr("{}.ko".format(TARGET_HOST_ID))
        logging.error("Unexpected error: {}".format(e))
        logging.error(traceback.format_exc())
        abort(502)


