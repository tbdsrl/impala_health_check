# -*- coding: utf-8 -*-

import logging

from functools import wraps
from impala.error import RPCError

from impala.dbapi import connect
# from django.conf import settings
# from django.db import connection

# from statsd.defaults.django import statsd


class ImpalaException(RPCError):
    pass


class ConcurrentRWException(ImpalaException):
    pass


class TableMetadataException(ImpalaException):
    pass


def parse_data(data):
    return [row.split('\t') for row in data]


def wrap_impala_client_exception(f):
    def decorate(*args, **kwargs):
        try:
            result = f(*args, **kwargs)
            # statsd.incr("ImpalaDBWrapper.query.success")
            return result
        except RPCError as exc:
            # statsd.incr("ImpalaDBWrapper.query.fail.RPCError")
            if 'Failed to load metadata for table' in str(exc):
                raise TableMetadataException(str(exc))
            if 'Failed to open HDFS file hdfs://' in str(exc):
                raise ConcurrentRWException(str(exc))
            raise
        except Exception as exc:
            # statsd.incr("ImpalaDBWrapper.query.fail.{}".format(exc))
            raise

    return decorate


class ImpalaDBWrapper(object):
    backend = 'impala'

    def _can_execute(self, query):
        return True

    def __init__(self, host, port, timeout=3):
        logging.debug("getting connection to {}:{} ...".format(host, port))
        port = int(port)
        self.conn = connect(host=host, port=port, timeout=timeout)
        logging.warn("connected to {}:{}".format(host, port))

    @wrap_impala_client_exception
    def execute(self, query, parameters=None, add_strip=False):
        logging.debug("executing query {} ...".format(query))
        self._can_execute(query)
        cursor = self.conn.cursor()
        cursor.execute(query, parameters=parameters)
        logging.debug("executed query {}".format(query))
        return cursor

    def close_connection(self):
        self.conn.close()

    @wrap_impala_client_exception
    def parse_data(self, data):
        parsed_data = []
        for line in data.fetchall():
            parsed_data.append([(hasattr(e, 'decode') and e.decode('utf-8') or e) for e in line])
        return parsed_data


_DB_WRAPPER = ImpalaDBWrapper

# This is an hack to instnziate the wrapper each request so we can use the settings
# decorator in the tests
def DBWrapper(host, port, timeout):
    return globals().get("_DB_WRAPPER")(host, port, timeout)


def ImpalaCursorWrapper():
    return ImpalaDBWrapper()


# def test():
#     import time
#     import random
#     from utility.utils import DBWrapper
#     from django.conf import settings
#     impalas = ['10.240.0.14', '10.240.0.128', '10.240.0.36', '10.240.0.135', '10.240.0.111', '10.240.0.105', '10.240.0.124', '10.240.0.122', '10.240.0.87', '10.240.0.47','10.240.0.112', '10.240.0.99', ]
#     settings.CDH5_IMPALA_ADDRESS = '{}:21050'.format(random.choice(impalas))
#     print settings.CDH5_IMPALA_ADDRESS
#     s = time.time()
#     DBWrapper().parse_data(DBWrapper().execute("""SELECT 1 as savemu, count(*) as field from administration_hourly_report_smart where time_p > 201710000100"""))
#     print time.time() - s