.. These are the Travis-CI and Coveralls badges for your repository. Replace
   your *github_repository* and uncomment these lines by removing the leading
   two dots.

.. .. image:: https://travis-ci.org/*github_repository*.svg?branch=master
    :target: https://travis-ci.org/*github_repository*

.. .. image:: https://coveralls.io/repos/github/*github_repository*/badge.svg?branch=master
    :target: https://coveralls.io/github/*github_repository*?branch=master


A simple http proxy to check if the impala deamon is connected to the cluster

sample run : 

    FLASK_APP=impala_health_check flask run --host=0.0.0.0 --port=24999

Verify the response is 200 ok 

    curl http://localhost:24999/health
    
This url will return non 200 response if the deamon is not running correcly or it is no ready to accept connections