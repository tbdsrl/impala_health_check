=============
API Reference
=============

API documentation for the impala_health_check module.

.. automodule:: impala_health_check
   :members: