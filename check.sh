#!/usr/bin/env bash

hosts="10.240.0.14 10.240.0.128 10.240.0.36 10.240.0.135 10.240.0.111 10.240.0.105 10.240.0.124 10.240.0.122 10.240.0.87 10.240.0.47 10.240.0.112 10.240.0.99"
#set -e

for host in $hosts; do
    echo "checking $host ..."
    curl --max-time 4 http://${host}:24999/health || true
    echo ""
done